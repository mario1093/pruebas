angular.module("modVenadoTestService", ["ngResource"])
.factory('VenadoTestApi', ["$resource", function($resource) {
    return $resource("https://venados.dacodes.mx/api/:methodName", {}, {
		gamer: {
			headers: {
                "accept": "application/json",
            },
            method: "GET"
            , params: {
                methodName: "games"
            }

            , isArray: false
        },
		estadistica: {
			headers: {
                "accept": "application/json",
            },
            method: "GET"
            , params: {
                methodName: "statistics"
            }

            , isArray: false
        },
		jugadres: {
			headers: {
                "accept": "application/json",
            },
            method: "GET"
            , params: {
                methodName: "players"
            }

            , isArray: false
        },
		notificacines: {
			headers: {
                "accept": "application/json",
            },
            method: "GET"
            , params: {
                methodName: "notifications"
            }

            , isArray: false
        }
		
    });
}]);
