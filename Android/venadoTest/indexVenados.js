angular.module("modVenadoTest", ["ngRoute", "ngResource", "modVenadoTestService"])
    .controller("VenadoTestCtrl", function ($scope, $rootScope, $http, VenadoTestApi, $filter) {

		moment.locale('es');
		$scope.banderaConsulta = true;
		$scope.listaNotificacines = [];
		$scope.listaCopaMx = [];
		$scope.listaEstadisticas = [];
		$scope.jugadores = null;
		$scope.jugador = null;

		$scope.valorVariables = function(title, valorInicio, valorEstad, valorJugador) {
			$scope.tituloNombre = title;
			$scope.mostrarInicio = valorInicio;
			$scope.mostrarEstadistica = valorEstad;
			$scope.mostrarJugador = valorJugador;
		}
		
		$scope.obtenerNotificaciones = function() {
			VenadoTestApi.notificacines(function (nti) {
				$scope.listaNotificacines = nti.data.notifications;
			});
		};
		
		//Consulta de Inicio
		$scope.inicio = function(value) {
			$scope.banderaConsulta = value;
			$scope.valorVariables("Venados F.C.", true, false, false);
			$scope.obtenerNotificaciones();
			VenadoTestApi.gamer(function (response) {
				var listCopamx = [];
				
				var listaGeneral = response.data.games;
				
				if(value){
					$("#btnCopaMx").addClass("div_hover");
					$("#btnAsensoMx").removeClass("div_hover");
					listCopamx = $filter('filter')(listaGeneral, {league: "Copa MX"});
				} else {
					$("#btnAsensoMx").addClass("div_hover");
					$("#btnCopaMx").removeClass("div_hover");
					listCopamx = $filter('filter')(listaGeneral, {league: "Ascenso MX"});
				}
				
				$scope.listaCopaMx = $scope.agruparMes(listCopamx);
			});
		};
		
		$scope.agruparMes = function(lista) {
			var listaAgrupado = [];
			
			angular.forEach(lista, function(equip){
				var mes = moment(equip.datetime).add(1, 'days').format('MMMM');
				mes = mes.toUpperCase();
				var objeto = {agrupador: mes, listaCopaMx: []};
				
				var agregar = true;
				var posicion = null;
				angular.forEach(listaAgrupado, function(data, key){
					if (data.agrupador == mes) {
						agregar = false;
						posicion = key;
					}
				});
				
				if (agregar) {
					objeto.listaCopaMx.push(equip);
					listaAgrupado.push(objeto);
				} else {
					if (posicion != null) {
						listaAgrupado[posicion].listaCopaMx.push(equip);
					}
				}	
			});
			
			return listaAgrupado;
		};
		
		$scope.diaMes = function(fecha) {
			return moment(fecha).add(1, 'days').format('D dddd').toUpperCase();
		};
       
		//Consulta de estadisticas
		$scope.obtenerEstadisticas = function() {
			$scope.listaEstadisticas = [];
			$scope.valorVariables("Estadísticas", false, true, false);
			VenadoTestApi.estadistica(function (response) {
				if(response.data.statistics > 0){
					$scope.listaEstadisticas = response.data.statistics;
				} else {
					//Agrege un objeto por default para mostrar la presentación.
					var objeto = {numero: 1, imagen: "imagenes/venados.png", equipo: "Venados F.C.", juegoJugado: 2, goles: 5, puntos: 6,};
					$scope.listaEstadisticas.push(objeto);
				}
			});
		};
		
		//Consulta de jugadres
		$scope.obtenerJugadres = function() {
			$scope.valorVariables("Jugadores", false, false, true);
			VenadoTestApi.jugadres(function (response) {
				
				var listPorterosMx = $filter('filter')(response.data.team.goalkeepers, {last_team: "Venados FC"});
				var listDefensasMx = $filter('filter')(response.data.team.defenses, {last_team: "Venados FC"});
				var listCenterMx = $filter('filter')(response.data.team.centers, {last_team: "Venados FC"});
				var listDelanterosMx = $filter('filter')(response.data.team.forwards, {last_team: "Venados FC"});
				
				var objeto= {listPorteros: listPorterosMx, listDefensas: listDefensasMx, listCenter: listCenterMx, listDelanteros: listDelanterosMx};
				
				$scope.jugadores = objeto;
			});
		};
		
		$scope.verDetalleJugador = function(jugador) {
			$scope.fechaNacimiento = moment(jugador.birthday).add(1, 'days').format('DD/MM/YYYY');
			$scope.jugador = jugador;
		};
		
    });


