﻿
Creado por: Carlos Mario López Rodríguez.

Aplicación web


Herramientas para la aplicación web:

- SmartAdmin v4.0.1 (Bootstrap 4, html5, CSS)

- AngularJs-1.4.2

- Jquery-2.1.4 (Se utiliza directo de internet)

- moment.js/2.19.2 (Se utiliza directo de internet)


Pasos para construir la aplicación web:

1.- Se crea Una carpeta principal con el nombre de "venadoTest".

2.- A nivel de la carpeta principal (venadoTest) se crean otras carpetas para organizar los archivos de la aplicación, con los nombres siguientes:

* Carpeta "lib": Contienen todas las librerías que se ocuparan en el sistema (SmartAdmin, AngularJs, etc).

* Carpeta "js": Contienen los archivos que contienen las api, en este caso se agregó solo 1 archivo con el nombre venadoService.js (contiene los métodos: "GET")

* Carpeta "imagenes": Contienen las imágenes que se ocuparan en el sistema o aplicación.

3.- A nivel de la carpeta principal (venadoTest) se crear dos archivos con los siguientes nombres:

* indexVenado.html: Plantilla principal de la aplicación web:

- header: se agregan todos los archivos que se ocuparan (angularJs, smartAdmin, etc.),

- body: se agregan todo el contenido a visualizar en el sistema.

- footer: datos destacados de la aplicación (página web, contacto, etc.).

* indexVenado.js: Archivo que contiene todo el código de funcionalidad (funciones, consultas ("GET") o variables) que se ocuparan para mostrar datos dinámicos 
en la plantilla principal (indexVenado.html).


Pasos para correr la aplicación web:

1.- Instalar el servidor Xampp.

2.- Buscar la carpeta que creo la instalación de xampp ejemplo: C:\xampp

3.- Alojar el proyecto de venados test (carpeta principal del proyecto) en la carpeta de htdocs de xampp, ejemplo: C:\xampp\htdocs 
Así se vería la ruta del proyecto alojado C:\xampp\htdocs\venadoTest.

4.- Este paso no es requerido, configurar el puerto del servidor apache que se ocupara para el proyecto de venados test:

* Ubicarse en la carpeta que genero la instalación de xampp y buscar una carpeta con el nombre de apache (C:\xampp\apache) y abrir la carpeta conf(C:\xampp\apache\conf), 
dentro de la carpeta conf abrir el archivo httpd.conf para modificar los siguientes texto la cual tendrá el puerto 8080:

-Listen 8080 (puerto a ocupar) 

-ServerName localhost:8080 (puerto a ocupar).


5.- Abrir la aplicacion XAMPP y ejecutar el servidor apache (presionar el botón strat, se pondrá en verde el texto apache).

6.- Abrir el navegador Google Chrome (instalar en caso que no se tenga), agregar el plugin CORS al navegador y abilitarlo(el plugin CORS se ocupa para poder 
realizar las consulta de las apis proporcionadas).

7.- Después de haber habilitado el pligin en el navegador, se escribe ruta del servidor o aplicación: localhost:puerto/carpetaProyecto/plantilla.html, 
ejemplo: localhost:8080/venadoTest/indexVenado.html

8.- Una vez accedido se puede navegar en la aplicación web de venado test.





