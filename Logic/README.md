Creado por: Carlos Mario L�pez Rodr�guez.

Aplicaci�n de logica de recorrido.


Herramientas para la creaci�n:

- Lenguaje de programaci�n Java versi�n 8

- IDE NetBeans versi�n 8.1

Nota: Es indispensable las herramientas para poder correr la aplicaci�n y en caso que no se tenga instalado java, instalarlos y configurar las variables de entorno.


Pasos para construir la aplicaci�n:

1.- En el IDE de NetBeans, crear una nueva aplicaci�n de tipo "Java" y "Java Application", la cual se le dio el nombre de "LogicaRecorrido".

2.- Se genera por default la Clase principal con el nombre "LogicaRecorrido.java" y con  un m�todo llamado "main", el m�todo main es el que nos permite correr la aplicaci�n.

3.- Se genera un nuevo m�todo dentro de la clase principal con el nombre "construirMatriz()", la cual se invoca desde el m�todo "main()".

4.- En el m�todo "construirMatriz()" est�n todas la variable a ocupar y los m�todos a ocupar para la validaci�n y recorrido de la matriz.


Pasos para correr la aplicaci�n.

1.- En el IDE de NetBeans se abre el proyecto de "LogicaRecorrido".

2.- Se abre la case principal del proyecto "LogicaRecorrido.java" y se correr la aplicaci�n o proyecto en el bot�n "Run Project".

3.- Al correr la aplicaci�n abre una venta, la cual nos pide que se ingrese el n�mero de fila o columna, para el tama�o de la matriz que tendr� nuestro recorrido en la cuadrilla,
en caso que no se ingrese un dato correcto no dejara proceder el proceso hasta ingresar correcto los datos.

4.- Al ingresar los datos correctos nos da el resultado de la posici�n en la que nos encontramos al finalizar el recorrido de la cuadrilla y  validar si queremos otro recorrido o finalizar el proceso.


Nota: Por cada recorrido de la cuadrilla(Proceso) se imprime los datos del recorrido en la terminal del IDE NetBeans, no se imprime en la ventana de "resultado" 
para evitar detalle cuando el usuario ingrese un numero de fila o columna muy grade ejemplo: fila = 100 y columna = 90.



