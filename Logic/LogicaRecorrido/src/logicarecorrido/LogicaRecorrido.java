/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logicarecorrido;

import java.util.Scanner;
import javax.swing.JOptionPane;

/**
 *
 * @author SIAEDC00050
 */
public class LogicaRecorrido {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //Metodo principal para construir la matriz.
        construirMatriz();
        
    }
    
    //Metodo para la contrucción de la matriz.
    private static void construirMatriz() {
        //Variables a utilizar.
        String matriz [][], resultado [];
        int fila, columna, cont = 1; 
        //int cont = 1;
        //String resultado [];
        
        fila = validarCampo("fila", true, JOptionPane.INFORMATION_MESSAGE);
        columna = validarCampo("columna", true, JOptionPane.INFORMATION_MESSAGE);
        
        resultado= new String [fila * columna];
        matriz = new String[fila][columna];
        
        //Metodo para inicializar el recorrido
        datoIzquierdaDerecha(0, 0, matriz, columna, fila, cont, resultado);
        
        //Recorrido para mostrar los datos del recorrido.
        System.out.println("Vista del recorrido de " + fila + " filas y " + columna + "columnas:");
        for (int fil = 0; fil < fila; fil++) {
            for (int col = 0; col < columna; col++) {
                System.out.print(matriz[fil][col]);
            }
            System.out.println(" ");
        }
        
        mensajeSalida(resultado[resultado.length - 1]);
    }
    
    //Metodo para validar si el dato es numerico, en caso que no lo sea pedir un dato correcto.
    private static int validarCampo(String campo, Boolean bandera, int icono) {
        
        String campoValor = JOptionPane.showInputDialog(null, "Escriba un número de " + campo + ":", "Catura de Datos", icono);
        
        if(isNumeric(campoValor)){
            return Integer.parseInt(campoValor);
        } else if(campoValor != null) {
            if (bandera) {
                campo = campo + " valido";
                bandera = !bandera;
                icono = JOptionPane.ERROR_MESSAGE;
            }
            
            return validarCampo(campo, bandera, icono);
        } else {
            System.exit(0); //Finaliza el proceso cuando se preciona el botón cancelar.
            return Integer.parseInt(campoValor);
        }
    }
    
    //Metodo para validar si el dato ingresado es numerico.
    private static boolean isNumeric(String cadena) {
        boolean resultado;

        try {
            Integer.parseInt(cadena);
            resultado = true;
        } catch (NumberFormatException excepcion) {
            resultado = false;
        }

        return resultado;
    }
    
    //Metodo para el mensaje de salida o resulta y si se desea realizar una nueva operación.
    private static void mensajeSalida(String resultado) {
        
        int mensaje = JOptionPane.showConfirmDialog(null, "Quedo posicionado mirando hacia: " + resultado + "\n\n¿Desea realizar un nuevo recorrido?", 
                "Resultado", JOptionPane.YES_NO_OPTION);
        
        if (mensaje == JOptionPane.YES_OPTION) {
            construirMatriz();
        }
    }
    
    //Metodos para construir el recorrido de la matriz.
    /*Recorrido dinamico de izquierda a derecha, y doblar a la derecha en la posición actual en caso de llegar al limite de columna 
    o si la columna a visitar ya fue visitado, en caso contrario mantenerse en la posición actual.*/
    private static void datoIzquierdaDerecha(int posicionFil, int posicionCol, String matriz [][], int columna, int fila, int cnt, String [] resultado) {
        Boolean continuar = false;
        int posicion = 0;
        for (int col = posicionCol; col < (columna); col++) {
            if (matriz[posicionFil][col] == null) {
                resultado [cnt-1] = "Derecha";
                matriz[posicionFil][col] = cnt++ + " \u2192 \t";
                continuar = true;
                posicion = col;
            } 
        }
        
        if (continuar) {
            //matriz[posicionFil][posicion] = cnt-1 + " \u2194 \t";
           datoDerechaAbajo(posicion, (posicionFil + 1), matriz, columna, fila, cnt, resultado);
        }
        
    }
    
    /*Recorrido dinamico de derecha hacia abajo, y doblar a la derecha en la posición actual en caso de llegar al limite de columna 
    o si la columna a visitar ya fue visitado, en caso contrario mantenerse en la posición actual.*/
    private static void datoDerechaAbajo(int posicionCol, int posicionFil, String matriz [][], int columna, int fila, int cnt, String [] resultado) {
        Boolean continuar = false;
        int posicion = 0;
        for (int fil = posicionFil; fil < fila; fil++) {
            if (matriz[fil][posicionCol] == null) {
                resultado [cnt-1] = "Abajo";
                matriz[fil][posicionCol] = cnt++ + " \u2193 \t";
                continuar = true;
                posicion = fil;
            } 
        }
        
        if (continuar) {
           //matriz[posicion][posicionCol] = cnt-1 + " \u21B5 \t";
           datoDerechaIzquierda(posicion, (posicionCol - 1), matriz, columna, fila, cnt, resultado);
        }
    }
    
    /*Recorrido dinamico de derecha a izquierda, y doblar a la derecha en la posición actual en caso de llegar al limite de columna 
    o si la columna a visitar ya fue visitado, en caso contrario mantenerse en la posición actual.*/
    private static void datoDerechaIzquierda(int posicionFil, int posicionCol, String matriz [][], int columna, int fila, int cnt, String [] resultado) {
        Boolean continuar = false;
        int posicion = 0;
        for (int col = posicionCol; col >= (columna - columna); col--) {
            if (matriz[posicionFil][col] == null) {
                resultado [cnt-1]= "Izquierda";
                matriz[posicionFil][col] = cnt++ + " \u2190 \t";
                continuar = true;
                posicion = col;
            } 
        }
        
        if (continuar) {
           datoIzquierdaArriba(posicion, (posicionFil - 1), matriz, columna, fila, cnt, resultado);
        }
    }
    
    /*Recorrido dinamico de izquierda hacia arriba, y doblar a la derecha en la posición actual en caso de llegar al limite de columna 
    o si la columna a visitar ya fue visitado, en caso contrario mantenerse en la posición actual.*/
    private static void datoIzquierdaArriba(int posicionCol, int posicionFil, String matriz [][], int columna, int fila, int cnt, String [] resultado) {
        Boolean continuar = false;
        int posicion = 0;
        
        for (int fil = posicionFil; fil >= (fila - (posicionFil + 1)); fil--) {
            if (matriz[fil][posicionCol] == null) {
                resultado [cnt-1]= "Arriba";
                matriz[fil][posicionCol] = cnt++ + " \u2191 \t";
                continuar = true;
                posicion = fil;
            } 
        }
        
        if (continuar) {
           datoIzquierdaDerecha(posicion, (posicionCol + 1), matriz, columna, fila, cnt, resultado);
        }
    }
    
}
