# Prueba iOS

### Descripción
La siguiente prueba consiste en construir un aplicación que muestre un listado de las películas recientes y su detalle.

### Instrucciones
- Utilizar Git para registrar el avance en las tareas.
- El diseño de las pantallas se debe ajustar a las diferentes resoluciones de los dispositivos iPhone.
- Al finalizar la prueba deberá contener un archivo README que explique exactamente los pasos necesarios para construir y ejecutar el proyecto.
- Tienes 24 horas para realizar el proyecto.

##### Recursos
Para poder obtener la información de las películas se conectará a la API de [The Movie DB](https://www.themoviedb.org/), para las diferentes llamadas a los endpoints se proporciona el siguiente API key:

> `API Key: 634b49e294bd1ff87914e7b9d014daed`

Para poder obtener las imagenes de las películas, se deben seguir los pasos que vienen en el siguiente link:

> [https://developers.themoviedb.org/3/getting-started/images](https://developers.themoviedb.org/3/getting-started/images)

#### Listado de películas
Esta pantalla debe mostrar el listado a dos columnas de las películas que estan actualmente en cartelera, para ello se debe llamar al sigueinte endpoint:

> [https://developers.themoviedb.org/3/movies/get-now-playing](https://developers.themoviedb.org/3/movies/get-now-playing)

El resultado esperado para esta pantalla, es como se muestra en la siguiente imagen:

![movies_list](images/test_ios_01.png)


#### Detalle película
Esta pantalla debe mostrar el detalle de la película seleccionada, el cual debe incluir:
- Imagen
- Título
- Duración
- Fecha de estreno
- Clasificación
- Géneros
- Descripción 

El endpoint que se debe llamar es el sigueinte:
> [https://developers.themoviedb.org/3/movies/get-movie-details](https://developers.themoviedb.org/3/movies/get-movie-details)

El resultado esperado para esta pantalla, es como se muestra en la siguiente imagen:

![movie_detail](images/test_ios_02.png)
